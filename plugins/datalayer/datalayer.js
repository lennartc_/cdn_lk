
/*
jQuery( document ).on( 'click', '.add_to_cart_button:not(.product_type_variable, .product_type_grouped, .single_add_to_cart_button)', function() {
        var productdata = jQuery( this ).closest( '.product' ).find( '.gtm4wp_productdata' );
        window.dataLayer.push({
            'event': 'add_to_cart',
            'productName': productdata.data( 'gtm4_product_name' ),
            'productSKU': jQuery( this ).data( 'product_sku' ),
            'productID': jQuery( this ).data( 'product_id' ),
        });
    });
*/
//$('[name=gtm_productdata]').data("gtm4_id")
    jQuery( document ).on( 'click', '.single_add_to_cart_button', function() {
        var _product_form     = jQuery( this ).closest( 'form.cart' );
     var _product_id       = jQuery('[name=gtm_productdata]', _product_form).data("gtm_id");
        //  var _product_id       = jQuery( '[name=gtm4_id]', _product_form ).val();
//        var _product_name     = jQuery( '[name=gtm4_name]', _product_form ).val();
        var _product_name       = jQuery('[name=gtm_productdata]', _product_form).data("gtm_name");
        //var _product_sku      = jQuery( '[name=gtm4_sku]', _product_form ).val();
        var _product_sku       = jQuery('[name=gtm_productdata]', _product_form).data("gtm_sku");
        //var _product_price     = jQuery( '[name=gtm4_price]', _product_form ).val();
        var _product_price       = jQuery('[name=gtm_productdata]', _product_form).data("gtm_price");
        //var _product_cat    = jQuery( '[name=gtm4_category]', _product_form ).val();
        var _product_cat       = jQuery('[name=gtm_productdata]', _product_form).data("gtm_category");
//        var _currency    = jQuery( '[name=gtm4_currency]', _product_form ).val();
        var _currency       = jQuery('[name=gtm_productdata]', _product_form).data("gtm_currency");
        //var _brand    = jQuery( '[name=gtm4_brand]', _product_form ).val();
        var _brand       = jQuery('[name=gtm_productdata]', _product_form).data("gtm_brand");
        var _quantity = $('.cw_qty').find(":selected").text() ? $('.cw_qty').find(":selected").text()  : '1';
        var _product_is_grouped = jQuery( _product_form ).hasClass( 'grouped_form' );
        if ( ! _product_is_grouped ) {
     /*  GA4     window.dataLayer.push({
                'event': 'add_to_cart',
                'ecommerce': {
                    'currency': _currency,
                    'items': [{
                        'item_id': _product_sku,
                        'item_name': _product_name,
                        'item_brand': _brand,
                        'price': _product_price,
                        'category': _product_cat,
                        'quantity': _quantity
                    }]
                }
            });*/
            dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                    'currencyCode': _currency,
                    'add': {                                // 'add' actionFieldObject measures.
                        'products': [{                        //  adding a product to a shopping cart.
                            'name': _product_name,
                            'id': _product_sku,
                            'price': _product_price,
                            'brand': _brand,
                            'category': _product_cat,
                            //'variant': 'Gray',
                            'quantity': _quantity
                        }]
                    }
                }
            });
        }
    });