
/*--------------------------------------------------------------
Load after document
--------------------------------------------------------------*/
window.onload = function() {
    // Run sal
    sal({threshold: 0.3, once: false,});

    //Footer cta
    $('.showhide').click(function(){
        $('.link').removeClass('show');
        Cookies.set('cta_besked', 1, { expires: 5 })
    });

    if ($('.link').hasClass("show")) {
        $('.scrollup').addClass('highup');
    }

    //open search at mobile
    $('.search_icon.shop-icon').click(function() {
        $('.searchbar').toggleClass('show');
    });

    /*--------------------------------------------------------------
    Flick: Produkt rows
    --------------------------------------------------------------*/
    if ($('.carousel-products').length) {
        var options = {
            pageDots: false,
            freeScroll: true,
            contain: true,
            // groupCells: 2,
            wrapAround: true,
            cellAlign: "left",
            prevNextButtons: false,
            autoPlay: false,
        }

// previous
        $('.button--previous').on('click', function () {
            $('.carousel-products').flickity('previous');
        });
// next
        $('.button--next').on('click', function () {
            $('.carousel-products').flickity('next');
        });

        $('.carousel-products').inViewport(function (px) {
            if (px) $(this).flickity(options);
            $(".carousel-cell").addClass("pageloaded");
            $(".slider-wrapper").addClass("animate");
        });
    }


// ADd fixed to header
    $(window).trigger('scroll');
    $(window).bind('scroll', function () {
        var pixels = 800; //number of pixels before modifying styles
        var pixels2 = 50; //number of pixels before modifying styles
        if ($(window).scrollTop() > pixels) {
            $('.head-wrap').addClass('fixed sticky-head ');
            //$('.head-wrap').removeClass('.head-top');
        } else if ($(window).scrollTop() < pixels2) {
            $('.head-wrap').removeClass('fixed sticky-head  ');
        }
    });

    /** ===========================================
     Hide / show the master navigation menu
     ============================================ */

        // console.log('Window Height is: ' + $(window).height());
        // console.log('Document Height is: ' + $(document).height());

    var previousScroll = 0;

    $(window).scroll(function () {

        var currentScroll = $(this).scrollTop();

        /*
         If the current scroll position is greater than 0 (the top) AND the current scroll position is less than the document height minus the window height (the bottom) run the navigation if/else statement.
         */
        if (currentScroll > 0 && currentScroll < $(document).height() - $(window).height()) {
            /*
             If the current scroll is greater than the previous scroll (i.e we're scrolling down the page), hide the nav.
             */
            if (currentScroll > previousScroll) {
                window.setTimeout(hideNav, 250);
                /*
                 Else we are scrolling up (i.e the previous scroll is greater than the current scroll), so show the nav.
                 */
            } else {
                window.setTimeout(showNav, 200);
            }
            /*
             Set the previous scroll value equal to the current scroll.
             */
            previousScroll = currentScroll;
        }

    });

    function hideNav() {
        $("[data-nav-status='toggle']").removeClass("is-visible").addClass("is-hidden");
    }
    function showNav() {
        $("[data-nav-status='toggle']").removeClass("is-hidden").addClass("is-visible");
    }

// Menu i top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('.sticky-head').removeClass('head-in-top');
            $('.sticky-head').addClass('head-roll');
        } else {
            $('.sticky-head').addClass('head-in-top');
            $('.sticky-head').removeClass('head-roll');

        }
    });

    /*--------------------------------------------------------------
Youtube
--------------------------------------------------------------*/

    if ($('.youtube').length) {
        (function () {
            var youtube = document.querySelectorAll(".youtube");
            for (var i = 0; i < youtube.length; i++) {
                var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/maxresdefault.jpg";
                var image = new Image();
                image.src = source;
                image.addEventListener("load", function () {
                    youtube[i].appendChild(image);
                }(i));
                youtube[i].addEventListener("click", function () {
                    var iframe = document.createElement("iframe");
                    iframe.setAttribute("frameborder", "0");
                    iframe.setAttribute("allowfullscreen", "");
                    iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?rel=0&showinfo=0&autoplay=1");
                    this.innerHTML = "";
                    this.appendChild(iframe);
                });
            }
        })();
    }

};
