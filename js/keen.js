/*--------------------------------------------------------------
Load after document
--------------------------------------------------------------*/
if ($('#hero-slider').length) {
    var sliderElement = document.getElementById("hero-slider");
    var interval = 0;
    function autoplay(run) {
        clearInterval(interval);
        interval = setInterval(() => {
            if (run && slider) {
                slider.next();
            }
        }, 6000);
    }

    var slider = new KeenSlider(sliderElement, {
        // slides: '.hero-slider__slide',
        vertical: false,
        loop: true,
        duration:2500,
        // adjust the speed that is translated to the slider when dragging
        //mode: "free-snap",
        easing: 500,
        // slide selector
        slidesPerView: 1,
        renderMode: 'performance'

        //   friction: 0.5,
    });



    sliderElement.addEventListener("mouseover", () => {
        autoplay(false);
    });
    sliderElement.addEventListener("mouseout", () => {
        autoplay(true);
    });
    sliderElement.addEventListener("touchstart", () => {
        autoplay(false);
    });
    sliderElement.addEventListener("touchend", () => {
        autoplay(true);
    });
    autoplay(true);
}


$(document).on('click', ".button--hero--next", function (e) {
    slider.next();
});

$(document).on('click', ".button--hero--previous", function (e) {
    slider.prev();
});

var ProductSlider = document.getElementById("product-slider");

var newslider = new KeenSlider(ProductSlider, {
    loop: true,
    duration:2500,
    slides: { perView:  "auto" },
})

$(document).on('click', ".button--next", function (e) {
    newslider.next();
});

$(document).on('click', ".button--previous", function (e) {
    newslider.prev();
});